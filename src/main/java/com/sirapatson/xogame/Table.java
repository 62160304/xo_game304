/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sirapatson.xogame;

/**
 *
 * @author Sirapatson
 */
public class Table {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastRow, lastCol;
    private Player winner;
    private boolean finish = false;
    private int sumTurn = 1;
    
    public Table(Player x, Player o){
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }
    
    public void showTable(){
        System.out.println(" 123");
        for(int i = 0; i < table.length; i++){
            System.out.print(i + 1);
            for(int j = 0; j < table[i].length; j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    public boolean setRowCol(int row, int col){
        if (table[row][col] == '-'){
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            return true;
        }
        return false;
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public void switchplayer(){
        if(currentPlayer == playerX){
            currentPlayer = playerO;
            sumTurn++;
        }else{
            currentPlayer = playerX;
            sumTurn++;
        }
    }
    
    public void checkCol(){
        for(int row = 0; row < 3; row++){
            if(table[row][lastCol] != currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void setStatWinLose() {
        if(currentPlayer == playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerO.lose();
            playerX.win();
        }
    }
    
    public void checkRow(){
        for(int col = 0; col < 3; col++){
            if(table[lastRow][col] != currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    
    public void checkLiftdiagonal(){
            if (table[0][0] == 'X') {//liftx
                if(table[1][1] == 'X'){
                    if(table[2][2] == 'X'){
                        finish = true;
                        winner = currentPlayer;
                        setStatWinLose();
                    }
                }
            }
            if (table[0][0] == 'O') {//lifto
                if(table[1][1] == 'O'){
                    if(table[2][2] == 'O'){
                        finish = true;
                        winner = currentPlayer;
                        setStatWinLose();
                    }
                }
            }
    }
    
    public void checkRightdiagonal(){
            if (table[0][2] == 'X') {//rightx
                if(table[1][1] == 'X'){
                    if(table[2][0] == 'X'){
                        finish = true;
                        winner = currentPlayer;
                        setStatWinLose();
                    }
                }
            }
            if (table[0][2] == 'O') {//righto
                if(table[1][1] == 'O'){
                    if(table[2][0] == 'O'){
                        finish = true;
                        winner = currentPlayer;
                        setStatWinLose();
                    }
                }
            }
    }
    
    public void checkDraw(){
        if(currentPlayer.getName() == '-' && sumTurn == 9){
            finish = true;
            playerO.draw();
            playerX.draw();
        }
    }
    
    public boolean isFinish(){
        return finish;
    }
    
    public void checkWin(){
        checkRow();
        checkCol();
        checkLiftdiagonal();
        checkRightdiagonal();
        checkDraw();
    }
    
    public Player getWinner(){
        return winner;
    }
    
}
